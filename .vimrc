set nocompatible                "vi互換の動作を無効　

" Vundle"
" 新しくインストールするときは :PluginInstall
"===================
let s:vundle_dir = expand('~/.vim/bundle/Vundle.vim')
if !isdirectory(s:vundle_dir)
  execute '!git clone https://github.com/VundleVim/Vundle.vim.git' s:vundle_dir
  endif

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'Shougo/unite.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'Shougo/neocomplete.vim'
Plugin 'Shougo/neosnippet'
Plugin 'Shougo/neosnippet-snippets'
Plugin 'justmao945/vim-clang'
Plugin 'thinca/vim-quickrun'
Plugin 'othree/yajs.vim'
Plugin 'ternjs/tern_for_vim'
Plugin 'tomtom/tcomment_vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'lervag/vimtex'
Plugin 'Chiel92/vim-autoformat'
Plugin 'gabrielelana/vim-markdown'

call vundle#end()
filetype plugin indent on

" 諸設定
set nobackup                    "バックアップファイルを作成しない
set noswapfile                  "スワップファイルを作成しない
" 検索
set hlsearch                    "検索結果をハイライト
set ignorecase                  "検索時に大文字小文字を区別しない
set smartcase                   "小文字で検索時に大文字小文字を区別しない
set infercase                   "補完時に大文字小文字を区別しない
" ステータスライン
set laststatus=2                "ステータスラインを常に表示
set ruler                       "カーソル行を強調表示
set number                      "行番号の表示
" cmdモード
set wildmenu                    "コマンドモードでファイル名補完した際に，候補を表示する
set showcmd                     "入力中のコマンドを表示
" indent
set autoindent                  "改行時に自動でインデント  
set smartindent                 "C言語ライクに自動でインデント
" tab
set shiftwidth=2                "自動インデントの際の空白の数
set shiftround                  "インデントをshiftwidthの倍数に丸める
set softtabstop=2               "<Tab>を押した際に挿入される空白の量
set expandtab                   "タブをスペースに置き換える
set tabstop=2                   "ファイル内の<Tab>が対応する空白の数
" folding
set foldmethod=indent           "インデントの数で折りたたみ
set foldlevel=30                "30以上のインデントで折りたたみ
" encoding
set encoding=utf-8
" other
set matchpairs& matchpairs+=<:> "対応する括弧をハイライト
set clipboard& clipboard+=unnamed "クリップボードへのコピーを可能に
set backspace=indent,eol,start "backspaceで文字を消せるように

" エイリアス
"=================
"ハイライトを消す
nnoremap <ESC><ESC> :noh<CR>
"~/.vimrc を開く
nnoremap vimrc :tabe ~/.vimrc<CR>
"NERDTree を実行
nnoremap tree :NERDTreeTabsToggle<CR>
"scp
cnoremap scp scp://user@IP:port/
"window 移動
nnoremap zh <C-w>h 
nnoremap zj <C-w>j
nnoremap zk <C-w>k
nnoremap zl <C-w>l
nnoremap zw <C-w>w

" vim grep 関係
nnoremap grep :vim<SPACE><SPACE>**\|cw<LEFT><LEFT><LEFT><LEFT><LEFT><LEFT>
nnoremap [N :cprevious<CR>
nnoremap [n :cnext<CR>

" タブ機能拡張
"===================
" Anywhere SID.
function! s:SID_PREFIX()
  return matchstr(expand('<sfile>'), '<SNR>\d\+_\zeSID_PREFIX$')
endfunction
" Set tabline.
function! s:my_tabline()  "{{{
  let s = ''
  for i in range(1, tabpagenr('$'))
    let bufnrs = tabpagebuflist(i)
    let bufnr = bufnrs[tabpagewinnr(i) - 1]  " first window, first appears
    let no = i  " display 0-origin tabpagenr.
    let mod = getbufvar(bufnr, '&modified') ? '!' : ' '
    let title = fnamemodify(bufname(bufnr), ':t')
    let title = '[' . title . ']'
    let s .= '%'.i.'T'
    let s .= '%#' . (i == tabpagenr() ? 'TabLineSel' : 'TabLine') . '#'
    let s .= no . ':' . title
    let s .= mod
    let s .= '%#TabLineFill# '
  endfor
  let s .= '%#TabLineFill#%T%=%#TabLine#'
  return s
endfunction "}}}
let &tabline = '%!'. s:SID_PREFIX() . 'my_tabline()'
set showtabline=2 " 常にタブラインを表示

" The prefix key.
nnoremap    [Tag]   <Nop>
nmap    1 [Tag]
" Tab jump
for n in range(1, 9)
  execute 'nnoremap <silent> [Tag]'.n  ':<C-u>tabnext'.n.'<CR>'
endfor
" t1 で1番左のタブ、t2 で1番左から2番目のタブにジャンプ
map <silent> [Tag]c :tablast <bar> tabnew<CR>
" tc 新しいタブを一番右に作る
map <silent> [Tag]x :tabclose<CR>
" tx タブを閉じる
map <silent> [Tag]n :tabnext<CR>
" tn 次のタブ
map <silent> [Tag]p :tabprevious<CR>
" tp 前のタブ



" --------------------------------
" neocomplete.vim
" --------------------------------
let g:acp_enableAtStartup = 0
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
" let g:neocomplcache_enable_camel_case_completion = 1
let g:neocomplcache_enable_underbar_completion = 1
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*' 

if !exists('g:neocomplete#force_omni_input_patterns')
  let g:neocomplete#force_omni_input_patterns = {}
endif
let g:neocomplete#force_overwrite_completefunc = 1

inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<S-TAB>"
" Close popup by <Space>.
inoremap <expr><Return> pumvisible() ? "\<C-y>" : "\<Return>"

" --------------------------------
" neosnippets
" --------------------------------
" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

"set snippet file dir
let g:neosnippet#snippets_directory='~/.cache/dein/.dein/neosnippets,~/.vim/snippets'

" --------------------------------
" NERDTree.vim
" --------------------------------
let file_name = expand('%')
if has('vim_starting') &&  file_name == ''
  autocmd VimEnter * NERDTree ./
endif
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_open_on_console_startup=1
let g:NERDTreeShowHidden=1
" 他のバッファが開いておらずNERDTreeだけの場合，閉じる
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" ---------------------------------
" 'justmao945/vim-clang' {{{
" ---------------------------------

" disable auto completion for vim-clanG
let g:clang_auto = 0
let g:clang_complete_auto = 0
let g:clang_auto_select = 0
let g:clang_use_library = 1

let g:clang_format_auto = 1
let g:clang_format_style = 'Google'

" default 'longest' can not work with neocomplete
let g:clang_c_completeopt   = 'menuone'
let g:clang_cpp_completeopt = 'menuone'

if executable('clang-3.6')
    let g:clang_exec = 'clang-3.6'
elseif executable('clang-3.5')
    let g:clang_exec = 'clang-3.5'
elseif executable('clang-3.4')
    let g:clang_exec = 'clang-3.4'
else
    let g:clang_exec = 'clang'
endif

if executable('clang-format-3.6')
    let g:clang_format_exec = 'clang-format-3.6'
elseif executable('clang-format-3.5')
    let g:clang_format_exec = 'clang-format-3.5'
elseif executable('clang-format-3.4')
    let g:clang_format_exec = 'clang-format-3.4'
else
    let g:clang_exec = 'clang-format'
endif

let g:clang_c_options = '-std=c11'
let g:clang_cpp_options = '-std=c++11 -stdlib=libc++'

let g:clang_check_syntax_auto = 0

""""""""""""""""""""""""""""""""""""""""""""""""""
" --------------------------------
" omnisharp-vim 
" --------------------------------
let g:OmniSharp_server_path = '~/omnisharp.http-osx/omnisharp/OmniSharp.exe'
let g:syntastic_cs_checkers = ['code_checker']
let g:OmniSharp_selector_ui = 'unite'  " Use unite.vim
let g:Omnisharp_stop_server = 1

""""""""""""""""""""""""""""""""""""""""""""""""""

" --------------------------------
" vim-markdown
" --------------------------------
let g:markdown_enable_spell_checking = 0


"""""""""""""""""""""""""""""""""""""""""""""""""
" --------------------------------
" vimtex
" --------------------------------
let g:vimtex_compiler_latexmk = {
      \ 'background': 1,
      \ 'build_dir': '',
      \ 'continuous': 1,
      \ 'options': [
      \    '-pdfdvi',
      \    '-verbose',
      \    '-file-line-error',
      \    '-synctex=1',
      \    '-interaction=nonstopmode',
      \],
      \}

let g:vimtex_view_general_options = '-r @line @pdf @tex'


syntax enable


""""""""""""""""""""""""""""""""""""""""""""""""""
" memo
""""""""""""""""""""""""""""""""""""""""""""""""""
" 置換
" :%s/hoge/fuga/g
